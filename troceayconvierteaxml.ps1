$filepath = "file.csv"

$file = Get-Item $filepath
$outfile = "operaciones{0}.xml"

$content = Get-Content $file
$csvheader = $content[0]
$lines = $content.Count
$minlines = 1000
$filepart = 1

$start = 1

# Per-operation template.
$docTemplate = @'
<operacionesModif>
  $($opers -join "`n")
</operacionesModif>
'@

# Per-operacion template.
$entryTemplate = @'
  <operacion>
      <cod_poperativo>$($oper.cod_poperativo)</cod_poperativo>
      <cod_operacion>$($oper.cod_operacion)</cod_operacion>
      <limitada_sobreprogramacion>$($oper.limitada_sobreprogramacion)</limitada_sobreprogramacion>
  </operacion>
'@

while ($start -lt $lines - 1) {
    #Set minimum $end value (last line)
    if ($start + $minlines -le $lines - 1) { $end = $start + $minlines - 1 } else { $end = $lines - 1 }

    #Value to compare. ColA is first column in my file = [0] .  ColB is second column = [1]
    $avalue = $content[$end].split(",")[0]
    #If not last line in script
    if ($end -ne $lines -1) {
        #Increase $end by 1 while ColA is the same
        while ($content[$end].split(",")[0] -eq $avalue) { $end++ }
        #Return to last line with equal ColA value
        $end--
    }
    #Create new csv-part
    $filename = $file.FullName.Replace($file.BaseName, ($file.BaseName + ".part$filepart"))
    $outfilename = $outfile -f (".part$filepart")
    @($csvheader, $content[$start..$end]) | Set-Content $filename

    Import-Csv $filename -Delimiter ';' | Group-Object cod_poperativo -ov grp | ForEach-Object {
      $opers = foreach ($oper in $_.Group) {
        # Instantiate the per-operacion template.
        $ExecutionContext.InvokeCommand.ExpandString($entryTemplate)  
      }

      # Instantiate the template, which encompasses
      # the per-operation elements, and output the result.
      $ExecutionContext.InvokeCommand.ExpandString($docTemplate)
    } | # Write the resulting XML document string to a file named for the user ID
      Set-Content -LiteralPath {  $outfilename }

    #Fix counters
    $filepart++
    $start = $end + 1
}


